package main.lab5;

import main.lab5.function.Function;
import main.lab5.input.FileInput;
import main.lab5.input.FunctionsMenuWindow;
import main.lab5.input.InputWindow;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

public class Application {
    private final JFrame frame;

    public Application(List<Function> functions) {
        frame = new JFrame("Lab 5");

        GroupLayout layout = new GroupLayout(frame.getContentPane());
        frame.setLayout(layout);

        JButton inputButton = new JButton("Ввести значения");
        JButton fileButton = new JButton("Считать из файла");
        JButton chooseButton = new JButton("Выбрать функцию");

        inputButton.setFont(new Font("Arial", Font.PLAIN, 24));
        inputButton.setBackground(Color.white);
        inputButton.setMinimumSize(new Dimension(300, 40));

        fileButton.setFont(new Font("Arial", Font.PLAIN, 24));
        fileButton.setBackground(Color.white);
        fileButton.setMinimumSize(new Dimension(300, 40));

        chooseButton.setFont(new Font("Arial", Font.PLAIN, 24));
        chooseButton.setBackground(Color.white);
        chooseButton.setMinimumSize(new Dimension(300, 40));

        inputButton.addActionListener(e -> new InputWindow());
        fileButton.addActionListener(e -> openFile());
        chooseButton.addActionListener(e -> new FunctionsMenuWindow(functions));

        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addComponent(inputButton)
                        .addComponent(fileButton)
                        .addComponent(chooseButton)
        );

        layout.setHorizontalGroup(
                layout.createParallelGroup()
                        .addComponent(inputButton)
                        .addComponent(fileButton)
                        .addComponent(chooseButton)
        );

        frame.getContentPane().setBackground(Color.WHITE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void openFile() {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
            try {
                FileInput input = new FileInput(fileChooser.getSelectedFile());
                new FunctionWindow(input.readPoints());
            }
            catch (IOException e) {
                JOptionPane.showMessageDialog(frame, "Ошибка открытия файла", "", JOptionPane.ERROR_MESSAGE);
            }
            catch (NumberFormatException | NoSuchElementException e) {
                JOptionPane.showMessageDialog(frame, "Ошибка чтения чисел из файла. Проверьте входные данные", "", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}