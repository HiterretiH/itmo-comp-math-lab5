package main.lab5.input;

import main.lab5.FunctionWindow;
import main.lab5.function.Function;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class FunctionsMenuWindow {
    private final JFrame frame;

    public FunctionsMenuWindow(List<Function> functions) {
        frame = new JFrame("Выбор функции");

        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.getContentPane().setBackground(Color.WHITE);

        Font font = new Font("Arial", Font.PLAIN, 24);

        JLabel countLabel = new JLabel("Количество отрезков:");
        JLabel fromLabel = new JLabel("От:");
        JLabel toLabel = new JLabel("До:");

        JTextField countField = new JTextField("4");
        JTextField fromField = new JTextField("1");
        JTextField toField = new JTextField("5");

        JLabel equationsLabel = new JLabel("Выберите функцию:");

        frame.add(countLabel);
        frame.add(countField);
        frame.add(Box.createVerticalStrut(5));

        frame.add(fromLabel);
        frame.add(fromField);

        frame.add(toLabel);
        frame.add(toField);
        frame.add(Box.createVerticalStrut(5));

        frame.add(equationsLabel);
        frame.add(Box.createVerticalStrut(5));

        for (Function function : functions) {
            JButton button = new JButton(function.getAsText());
            button.setFont(font);
            button.setBackground(null);

            button.addActionListener(e -> submit(
                    fromField.getText(),
                    toField.getText(),
                    countField.getText(),
                    function
            ));

            frame.add(button);
        }

        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setMinimumSize(new Dimension(200, 300));
        frame.setLocationRelativeTo(null);
    }

    private void submit(String fromText, String toText, String countText, Function function) {
        int size;
        double to, from;

        try {
            size = Integer.parseInt(countText);
        }
        catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(frame, "Количество отрезков должно быть целым числом", "", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            to = Double.parseDouble(toText);
            from = Double.parseDouble(fromText);
        }
        catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(frame, "Значения от и до должны быть числами", "", JOptionPane.ERROR_MESSAGE);
            return;
        }

        double[][] points = new double[size + 1][2];

        double step = (to - from) / size;
        for (int i = 0; i < size; i++) {
            points[i][0] = from;
            points[i][1] = function.f(from);
            from += step;
        }

        points[size][0] = to;
        points[size][1] = function.f(to);

        new FunctionWindow(points);
    }
}
