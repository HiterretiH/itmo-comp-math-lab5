package main.lab5.input;

import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class FileInput {
    private final Scanner scanner;

    public FileInput(File file) throws IOException {
        scanner = new Scanner(file);
    }

    public double[][] readPoints() throws NumberFormatException, NoSuchElementException {
        int count = readInt();

        double[][] points = new double[count][2];
        for (int i = 0; i < count; i++) {
            points[i][0] = readDouble();
            points[i][1] = readDouble();
        }

        return points;
    }

    public int readInt() throws NumberFormatException, NoSuchElementException {
        return Integer.parseInt(scanner.next());
    }

    public double readDouble() throws NumberFormatException, NoSuchElementException {
        return Double.parseDouble(scanner.next().replace(',', '.'));
    }
}
