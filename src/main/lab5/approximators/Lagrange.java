package main.lab5.approximators;

import main.lab5.function.Function;

public class Lagrange {
    private final double[][] points;
    private final int size;

    public Lagrange(double[][] points) {
        this.points = points;
        this.size = points.length;
    }

    public Function getFunction() {
        return new Function(
                "",
                "Многочлен Лагранжа",
                x -> {
                    double res = 0;
                    for (int i = 0; i < size; i++) {
                        double l = points[i][1];

                        for (int j = 0; j < size; j++) {
                            if (i == j) continue;

                            l *= (x - points[j][0]) / (points[i][0] - points[j][0]);
                        }

                        res += l;
                    }

                    return res;
                }
        );
    }
}
