package main.lab5.approximators;

import main.lab5.function.Function;

public class Newton implements NewtonInterface {
    private final double[][] points;
    private final int size;
    private final double[][] f;

    public Newton(double[][] points) {
        this.points = points;
        this.size = points.length;
        this.f = new double[size][size];
        build();
    }

    private void build() {
        for (int i = 0; i < size; i++) {
            f[0][i] = points[i][1];
        }

        for (int i = 1; i < size; i++) {
            for (int j = 0; j < size - i; j++) {
                f[i][j] = (f[i - 1][j + 1] - f[i - 1][j]) / (points[j + i][0] - points[j][0]);
            }
        }
    }

    public double[][] getDifferences() {
        return f;
    }

    public Function getFunction() {
        return new Function(
                "",
                "Многочлен Ньютона",
                x -> {
                    double res = f[0][0];

                    for (int i = 1; i < size; i++) {
                        double s = f[i][0];
                        for (int j = 0; j < i; j++) {
                            s *= (x - points[j][0]);
                        }
                        res += s;
                    }

                    return res;
                }
        );
    }
}
