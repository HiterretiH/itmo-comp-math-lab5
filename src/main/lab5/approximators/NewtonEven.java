package main.lab5.approximators;

import main.lab5.function.Function;

public class NewtonEven implements NewtonInterface {
    private final double[][] points;
    private final double step;
    private final int size;
    private final double[][] f;

    public NewtonEven(double[][] points) {
        this.points = points;
        this.size = points.length;
        this.step = (points[size - 1][0] - points[0][0]) / (size - 1);
        this.f = new double[size][size];
        build();
    }

    private void build() {
        for (int i = 0; i < size; i++) {
            f[0][i] = points[i][1];
        }

        for (int i = 1; i < size; i++) {
            for (int j = 0; j < size - i; j++) {
                f[i][j] = f[i - 1][j + 1] - f[i - 1][j];
            }
        }
    }

    public double[][] getDifferences() {
        return f;
    }

    public Function getFunction() {
        return new Function(
                "",
                "Многочлен Ньютона (равноотстоящие узлы)",
                x -> {
                    double res = f[0][0];
                    double factorial = 1;
                    double power = 1;

                    for (int i = 1; i < size; i++) {
                        factorial *= i;
                        power *= step;

                        double s = f[i][0] / factorial / power;
                        for (int j = 0; j < i; j++) {
                            s *= (x - points[j][0]);
                        }
                        res += s;
                    }

                    return res;
                }
        );
    }
}
