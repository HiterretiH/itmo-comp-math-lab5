package main.lab5.approximators;

import main.lab5.function.Function;

public interface NewtonInterface {
    Function getFunction();
    double[][] getDifferences();
}
