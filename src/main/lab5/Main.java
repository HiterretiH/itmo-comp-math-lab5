package main.lab5;

import main.lab5.function.FunctionLoader;

public class Main {
    public static void main(String... args) {
        new Application(FunctionLoader.getFunctions());
    }
}
