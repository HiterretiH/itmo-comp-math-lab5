package main.lab5;

import main.lab5.approximators.Lagrange;
import main.lab5.approximators.Newton;
import main.lab5.approximators.NewtonEven;
import main.lab5.approximators.NewtonInterface;
import main.lab5.function.Function;
import main.lab5.util.NumberFormatter;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.LegendItemEntity;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;

public class FunctionWindow {
    private final JFrame frame;
    private final double[][] points;
    private final Function[] functions;
    private final int size;
    private double from;
    private double to;
    private double min;
    private double max;

    public FunctionWindow(double[][] points) {
        frame = new JFrame("Результат");
        this.points = points;
        this.size = points.length;

        GroupLayout layout = new GroupLayout(frame.getContentPane());
        frame.setLayout(layout);
        layout.setAutoCreateContainerGaps(true);

        boolean equalStep = isEqualStep();
        NewtonInterface newton = equalStep ? new NewtonEven(points) : new Newton(points);

        functions = new Function[] {
                new Lagrange(points).getFunction(),
                newton.getFunction()
        };

        XYSeriesCollection dataset = new XYSeriesCollection();
        findRange();
        dataset.addSeries(getDotsSeries(points));
        for (Function function : functions) {
            dataset.addSeries(getSeries(function));
        }

        ChartPanel chart = generateChart(dataset);
        JTable table = getTable(equalStep, newton);
        JScrollPane tablePane = new JScrollPane(table);

        JLabel tableLabel = equalStep ? new JLabel("Конечные разности: ") : new JLabel("Разделенные разности: ");
        JLabel calculateLabel = new JLabel("Введите значение x: ");
        JTextField xField = new JTextField(Double.toString(points[0][0]));
        JButton calculateButton = new JButton("Рассчитать");
        JLabel resultLabel = new JLabel("");

        calculateButton.addActionListener(event -> {
            double x;
            try {
                x = Double.parseDouble(xField.getText());
            }
            catch (NumberFormatException e) {
                return;
            }

            StringBuilder res = new StringBuilder("<html>");
            for (Function function : functions) {
                res.append(function.getTitle()).append(": ").append(NumberFormatter.format(function.f(x))).append("<br>");
            }
            res.append("</html>");

            resultLabel.setText(res.toString());
        });
        calculateButton.doClick();

        layout.setVerticalGroup(layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                        .addComponent(chart)
                        .addGap(10)
                        .addGroup(layout.createParallelGroup()
                                .addComponent(calculateLabel)
                                .addComponent(xField, 15, 20, 25)
                        )
                        .addComponent(calculateButton)
                        .addComponent(resultLabel)
                )
                .addGroup(layout.createSequentialGroup()
                        .addComponent(tableLabel)
                        .addComponent(tablePane)
                )
        );

        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup()
                        .addComponent(chart)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(calculateLabel)
                                .addComponent(xField)
                        )
                        .addComponent(calculateButton)
                        .addComponent(resultLabel)
                )
                .addGap(10)
                .addGroup(layout.createParallelGroup()
                        .addComponent(tableLabel)
                        .addComponent(tablePane, table.getPreferredSize().width, table.getPreferredSize().width, table.getPreferredSize().width)
                )
        );

        frame.getContentPane().setBackground(Color.WHITE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    private JTable getTable(boolean equalStep, NewtonInterface newton) {
        String[] columnNames = new String[points.length + 1];
        String[][] data = new String[points.length][points.length + 1];

        double[][] f = newton.getDifferences();

        columnNames[0] = "<html>x<sub>i</sub></html>";

        for (int i = 0; i < points.length; i++) {
            data[i][0] = "<html>x<sub>" + i + "</sub></html>";
            for (int j = 0; j < points.length; j++) {
                if (j < points.length - i) {
                    data[i][j + 1] = NumberFormatter.format(f[i][j]);
                } else {
                    data[i][j + 1] = "";
                }
            }
        }

        if (equalStep) {
            for (int i = 0; i < points.length; i++) {
                if (i > 0) columnNames[i + 1] = "<html>&Delta;<sup>" + (i + 1) + "</sup> y<sub>i</sub></html>";
                else columnNames[i + 1] = "<html>&Delta; y<sub>i</sub></html>";
            }
        }
        else {
            for (int i = 0; i < points.length; i++) {
                if (i > 0) columnNames[i + 1] = "<html>f(x<sub>i</sub>, x<sub>i+" + i + "</sub>)</html>";
                else columnNames[i + 1] = "<html>f(x<sub>i</sub>)</html>";
            }
        }

        return new JTable(data, columnNames);
    }

    private boolean isEqualStep() {
        double step = (points[points.length - 1][0] - points[0][0]) / (points.length - 1);
        boolean equalStep = true;

        for (int i = 1; i < points.length; i++) {
            if (points[i][0] - points[i - 1][0] != step) {
                equalStep = false;
                break;
            }
        }

        return equalStep;
    }

    private void findRange() {
        from = points[0][0];
        to = points[0][0];
        min = points[0][1];
        max = points[0][1];

        for (int i = 0; i < size; i++) {
            if (points[i][0] > to) to = points[i][0];
            if (points[i][0] < from) from = points[i][0];
            if (points[i][1] < min) min = points[i][1];
            if (points[i][1] > max) max = points[i][1];
        }

        double xStep = (to - from) / size;
        double yStep = (max - min) / size;

        from -= xStep;
        to += xStep;
        min -= yStep;
        max += yStep;
    }

    private XYSeries getSeries(Function f) {
        double from = this.from - (this.to - this.from) * 0.25;
        double to = this.to + (this.to - this.from) * 0.25;

        XYSeries series = new XYSeries(f.getTitle());

        while (from <= to) {
            series.add(from, f.f(from));
            from += 0.03125;
        }

        return series;
    }

    private XYSeries getDotsSeries(double[][] points) {
        XYSeries series = new XYSeries("Точки");

        for (int i = 0; i < points.length; i++) {
            series.add(points[i][0], points[i][1]);
        }

        return series;
    }

    private ChartPanel generateChart(XYSeriesCollection dataset) {
        NumberAxis xAxis = new NumberAxis("X");
        NumberAxis yAxis = new NumberAxis("Y");

        xAxis.setRange(from, to);
        yAxis.setRange(min, max);

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer(true, true);
        renderer.setDefaultToolTipGenerator(new StandardXYToolTipGenerator());

        XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);
        JFreeChart chart = new JFreeChart("Функции", plot);

        int count = dataset.getSeriesCount();
        for (int i = 0; i < count; i++) {
            renderer.setSeriesShapesVisible(i, false);
            renderer.setSeriesLinesVisible(i, true);
        }
        renderer.setSeriesShapesVisible(0, true);
        renderer.setSeriesLinesVisible(0, false);

        return getChartPanel(dataset, chart, renderer);
    }

    private static ChartPanel getChartPanel(XYSeriesCollection dataset, JFreeChart chart, XYLineAndShapeRenderer renderer) {
        ChartPanel chartPanel = new ChartPanel(chart) {
            @Override
            public Dimension getPreferredSize() {
                return new Dimension(640, 480);
            }
        };

        chartPanel.setMouseWheelEnabled(true);
        chartPanel.setMouseZoomable(true, true);

        chartPanel.addChartMouseListener(new ChartMouseListener() {
            @Override
            public void chartMouseClicked(ChartMouseEvent e) {
                ChartEntity ce = e.getEntity();
                if (ce instanceof LegendItemEntity item) {
                    int index = dataset.getSeriesIndex(item.getSeriesKey());
                    boolean visible = renderer.getSeriesLinesVisible(index);
                    if (index == 0) return;

                    renderer.setSeriesLinesVisible(index, !visible);
                }
            }

            @Override
            public void chartMouseMoved(ChartMouseEvent e) {}
        });
        return chartPanel;
    }
}
