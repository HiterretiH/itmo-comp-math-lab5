package main.lab5.function;

import java.util.function.DoubleUnaryOperator;

public class Function {
    private final String text;
    private final String title;
    private final DoubleUnaryOperator f;

    public Function(String text, String title, DoubleUnaryOperator f) {
        this.text = text;
        this.title = title;
        this.f = f;
    }

    public String getAsText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    public double f(double x) {
        return f.applyAsDouble(x);
    }
}
