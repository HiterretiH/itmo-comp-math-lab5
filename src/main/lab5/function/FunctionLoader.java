package main.lab5.function;

import java.util.ArrayList;
import java.util.List;

public class FunctionLoader {
    public static List<Function> getFunctions() {
        List<Function> functions = new ArrayList<>(5);

        functions.add(new Function(
                "sin(x)",
                "Синус",
                Math::sin
        ));

        functions.add(new Function(
                "cos(x)",
                "Косинус",
                Math::cos
        ));

        functions.add(new Function(
            "sqrt(x)",
                "Корень",
                Math::sqrt
        ));

        functions.add(new Function(
                "ln(x)",
                "Логафим",
                Math::log
        ));

        functions.add(new Function(
                "exp(x)",
                "Экспонента",
                Math::exp
        ));

        return functions;
    }
}
